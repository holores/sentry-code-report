# Bitbucket Pipelines Pipe: Sentry Code Report

Generates a report of potential errors tracked in Sentry of the changed files as a build step in Bitbuket Pipelines.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:


```yaml
- pipe: sentryio/sentry-code-report:0.1.0
  variables:
    SENTRY_ORG: '<string>'
    SENTRY_PROJECT_ID: <number>
    # SENTRY_URL: '<string>' # Optional
```

## Variables

### Basic Usage

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| SENTRY_ORG (*) | The Sentry organization associated with the release. |
| SENTRY_PROJECT_ID (*) | The Sentry project id associated with the release. |
| SENTRY_URL | The Sentry url, only set when self hosted. |

_(*) = required variable._


Where you can find the `$SENTRY_PROJECT_ID`:

- Go to https://sentry.io/settings/
- Click on `Settings` in the main sidebar
- Click on `Projects` in the secondary sidebar and select your project
- Click on `Client Keys (DSN)` in the secondary sidebar under `SDK Setup`
- On a panel titled `Default`, click the `Configure` button
- Scroll to the bottom, and you will find the `Project ID`

## Examples

```yaml
- pipe: holores/sentry-code-report:0.1.0
  variables:
    SENTRY_ORG: 'sentry-org'
    SENTRY_PROJECT_ID: 12345
```

## Support

support@sentry.io