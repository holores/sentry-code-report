import os, json
from unittest.mock import patch
import responses
from pipe.insights import CodeInsights


class TestCodeInsights(object):
    @responses.activate
    @patch.dict(
        "os.environ",
        {
            "BITBUCKET_WORKSPACE": "my_workspace",
            "BITBUCKET_REPO_SLUG": "some_repo",
            "BITBUCKET_COMMIT": "some_commit",
            "BITBUCKET_PR_ID": "",
            "SENTRY_ORG": "sentry",
            "SENTRY_PROJECT_ID": "123",
        },
    )
    def test_fetch_diff_with_commit(self):
        responses.add(
            responses.GET,
            "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/diffstat/some_commit",
            json={},
            status=200,
        )

        resp = CodeInsights().fetch_diff()
        assert len(responses.calls) == 1
        assert (
            responses.calls[0].request.url
            == "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/diffstat/some_commit"
        )

    @responses.activate
    @patch.dict(
        "os.environ",
        {
            "BITBUCKET_WORKSPACE": "my_workspace",
            "BITBUCKET_REPO_SLUG": "some_repo",
            "BITBUCKET_COMMIT": "",
            "BITBUCKET_PR_ID": "some_pr_id",
            "SENTRY_ORG": "sentry",
            "SENTRY_PROJECT_ID": "123",
        },
    )
    def test_fetch_diff_with_pr_id(self, monkeypatch):
        responses.add(
            responses.GET,
            "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/pullrequests/some_pr_id/diffstat",
            json={},
            status=302,
        )

        resp = CodeInsights().fetch_diff()
        assert len(responses.calls) == 1
        assert (
            responses.calls[0].request.url
            == "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/pullrequests/some_pr_id/diffstat"
        )

    @responses.activate
    @patch.dict(
        "os.environ",
        {
            "BITBUCKET_WORKSPACE": "my_workspace",
            "BITBUCKET_REPO_SLUG": "some_repo",
            "BITBUCKET_COMMIT": "some_commit",
            "BITBUCKET_PR_ID": "",
            "SENTRY_ORG": "sentry",
            "SENTRY_PROJECT_ID": "123",
        },
    )
    def test_generate_report(self):
        # fetch diff
        responses.add(
            responses.GET,
            "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/diffstat/some_commit",
            json={
                "pagelen": 500,
                "values": [
                    {
                        "status": "modified",
                        "old": {
                            "path": "index.js",
                            "type": "commit_file",
                            "links": {
                                "self": {
                                    "href": "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/src/61b6579f5e5ceb49b084e13af0a9982afc252339/index.js"
                                }
                            },
                        },
                        "lines_removed": 1,
                        "lines_added": 1,
                        "new": {
                            "path": "index.js",
                            "type": "commit_file",
                            "links": {
                                "self": {
                                    "href": "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/src/f1be1e48b92ecd9de6fb6e2b8eef3f0d20d29083/index.js"
                                }
                            },
                        },
                        "type": "diffstat",
                    },
                    {
                        "status": "modified",
                        "old": {
                            "path": "server.js",
                            "type": "commit_file",
                            "links": {
                                "self": {
                                    "href": "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/src/61b6579f5e5ceb49b084e13af0a9982afc252339/server.js"
                                }
                            },
                        },
                        "lines_removed": 1,
                        "lines_added": 1,
                        "new": {
                            "path": "server.js",
                            "type": "commit_file",
                            "links": {
                                "self": {
                                    "href": "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/src/f1be1e48b92ecd9de6fb6e2b8eef3f0d20d29083/server.js"
                                }
                            },
                        },
                        "type": "diffstat",
                    },
                ],
                "page": 1,
                "size": 1,
            },
            status=200,
        )

        ## create report
        responses.add(
            responses.PUT,
            "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/commit/some_commit/reports/1",
            json={
                "uuid": "{356a192b-7913-404c-9457-4d18c28d46e6}",
                "details": "Check the files modified in this pull request for potential unresolved issues in Sentry.",
                "title": "Sentry Errors",
                "external_id": "1",
                "report_type": "COVERAGE",
                "created_on": "2020-03-06T00:39:02.744Z",
                "updated_on": "2020-03-06T00:39:02.744Z",
            },
            status=201,
        )

        ## create annotations 1
        responses.add(
            responses.PUT,
            "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/commit/some_commit/reports/1/annotations/2",
            json={
                "external_id": "2",
                "uuid": "{734057d0-6ab4-5c25-84f0-f753a8b62fa5}",
                "annotation_type": "BUG",
                "path": "index.js",
                "details": "Check Sentry for an unresolved issues",
                "link": "https://sentry.io/organizations/sentry/issues/?query=is%3Aunresolved+index.js&statsPeriod=14d",
                "created_on": "2020-03-06T00:39:03.072Z",
                "updated_on": "2020-03-06T00:39:03.072Z",
            },
            status=201,
        )

        ## create annotations 2
        responses.add(
            responses.PUT,
            "https://api.bitbucket.org/2.0/repositories/my_workspace/some_repo/commit/some_commit/reports/1/annotations/3",
            json={
                "external_id": "3",
                "uuid": "{734057d0-6ab4-5c25-84f0-f753a8b62fa5}",
                "annotation_type": "BUG",
                "path": "server.js",
                "details": "Check Sentry for an unresolved issues",
                "link": "https://sentry.io/organizations/sentry/issues/?query=is%3Aunresolved+server.js&statsPeriod=14d",
                "created_on": "2020-03-06T00:39:03.072Z",
                "updated_on": "2020-03-06T00:39:03.072Z",
            },
            status=201,
        )

        resp = CodeInsights().generate_report()
        assert len(responses.calls) == 4

        # Validate create report request body
        report_request = json.loads(responses.calls[1].request.body)
        assert report_request["title"] == "Sentry Errors"
        assert report_request["external_id"] == 1
        assert report_request["report_type"] == "COVERAGE"
        assert (
            report_request["details"]
            == "Check the files modified in this pull request for potential unresolved issues in Sentry."
        )

        # Validate create annotation request body
        annotation_request = json.loads(responses.calls[3].request.body)
        assert annotation_request["annotation_type"] == "BUG"
        assert annotation_request["external_id"] == 3
        assert annotation_request["summary"] == "Check Sentry for unresolved issues"
        assert annotation_request["path"] == "server.js"
        assert (
            annotation_request["link"]
            == "https://sentry.io/organizations/sentry/issues/?project=123&query=is%3Aunresolved+server.js&statsPeriod=90d"
        )
        assert annotation_request["line"] == 0
